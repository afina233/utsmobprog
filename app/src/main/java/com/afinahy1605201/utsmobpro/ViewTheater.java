package com.afinahy1605201.utsmobpro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afinahy1605201.utsmobpro.model.MovieDetail;
import com.afinahy1605201.utsmobpro.model.Theater;
import com.afinahy1605201.utsmobpro.utility.Utility;

import java.util.ArrayList;

public class ViewTheater extends AppCompatActivity {
    RecyclerView rvTheater;
    private ArrayList<Theater> list;

    public static String EXTRA_NAME = "extra_name";
    public static String EXTRA_RATING = "extra_rating";
    public static String EXTRA_RELEASE = "extra_release";
    public static String EXTRA_DETAIL = "extra_detail";
    public static String EXTRA_BACKDROP = "extra_backdrop";
    public static String EXTRA_POSTER = "extra_poster";

    public static String EXTRA_THEATER = "extra_theater";
    public static String EXTRA_LONGITUDE = "extra_longitude";
    public static String EXTRA_LATITUDE = "extra_latitude";



    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.theater_help);
        rvTheater = (RecyclerView)findViewById(R.id.theater);
        rvTheater.setHasFixedSize(true);
        list = new ArrayList<>();
        list.addAll(Utility.getListTheater());
        showRecyclerList();


    }

    public void onClick(View v){
        final String name = getIntent().getStringExtra(EXTRA_NAME);
        final String release = getIntent().getStringExtra(EXTRA_RELEASE);
        final String rating = getIntent().getStringExtra(EXTRA_RATING);
        final String detail = getIntent().getStringExtra(EXTRA_DETAIL);
        final String backdrop = getIntent().getStringExtra(EXTRA_BACKDROP);
        final String poster = getIntent().getStringExtra(EXTRA_POSTER);
        final String theater = ((TextView)findViewById(R.id.tv_item_name)).getText().toString();

        Intent intent = new Intent(ViewTheater.this, MapsActivity.class);
        intent.putExtra(MovieDetail.EXTRA_NAME, name);
        intent.putExtra(MovieDetail.EXTRA_RATING, rating);
        intent.putExtra(MovieDetail.EXTRA_RELEASE, release);
        intent.putExtra(MovieDetail.EXTRA_DETAIL, detail);
        intent.putExtra(MovieDetail.EXTRA_BACKDROP, backdrop);
        intent.putExtra(MovieDetail.EXTRA_POSTER, poster);
        ViewTheater.this.startActivity(intent);
    }

    private void showRecyclerList(){
        rvTheater.setLayoutManager(new LinearLayoutManager(this));
        ListTheaterAdapter listTheaterAdapter = new ListTheaterAdapter(this);
        listTheaterAdapter.setListTheater(list);
        rvTheater.setAdapter(listTheaterAdapter);
    }
}
