package com.afinahy1605201.utsmobpro.utility;

import com.afinahy1605201.utsmobpro.model.Theater;

import java.util.ArrayList;

public class Utility {

    public static ArrayList<Theater> getListTheater(){
        ArrayList<Theater> theaters = new ArrayList<Theater>();

        Theater tht = new Theater();
        tht.setName("Ciwalk XXI");
        tht.setLogo("https://lh3.googleusercontent.com/-UGzN8xMDrWs/T3Qh3zua0jI/AAAAAAAABLw/mONjXUOWF2M/w2362-h544-p/Logo_Cinema%2BXXI.jpg");
        tht.setLatitude(-6.893196);
        tht.setLongitude(107.6023293);
        theaters.add(tht);

        tht = new Theater();
        tht.setName("BTC XXI");
        tht.setLogo("https://lh3.googleusercontent.com/-UGzN8xMDrWs/T3Qh3zua0jI/AAAAAAAABLw/mONjXUOWF2M/w2362-h544-p/Logo_Cinema%2BXXI.jpg");
        tht.setLatitude(-6.8930026);
        tht.setLongitude(107.5827022);
        theaters.add(tht);

        tht = new Theater();
        tht.setName("BRAGA XXI");
        tht.setLogo("https://lh3.googleusercontent.com/-UGzN8xMDrWs/T3Qh3zua0jI/AAAAAAAABLw/mONjXUOWF2M/w2362-h544-p/Logo_Cinema%2BXXI.jpg");
        tht.setLatitude(-6.9171441);
        tht.setLongitude(107.6066858);
        theaters.add(tht);

        tht = new Theater();
        tht.setName("CGV BEC");
        tht.setLogo("https://upload.wikimedia.org/wikipedia/id/4/43/CGV_Logo_Global_BI_V9-02.png");
        tht.setLatitude(-6.9082098);
        tht.setLongitude(107.6067256);
        theaters.add(tht);

        tht = new Theater();
        tht.setName("CGV PVJ");
        tht.setLogo("https://upload.wikimedia.org/wikipedia/id/4/43/CGV_Logo_Global_BI_V9-02.png");
        tht.setLatitude(-6.8881615);
        tht.setLongitude(107.5934793);
        theaters.add(tht);

        tht = new Theater();
        tht.setName("CGV 23 PASKAL");
        tht.setLogo("https://upload.wikimedia.org/wikipedia/id/4/43/CGV_Logo_Global_BI_V9-02.png");
        tht.setLatitude(-6.9154162);
        tht.setLongitude(107.5920725);
        theaters.add(tht);

        return theaters;
    }


}
