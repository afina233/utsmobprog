package com.afinahy1605201.utsmobpro;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afinahy1605201.utsmobpro.model.Theater;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ListTheaterAdapter extends RecyclerView.Adapter<ListTheaterAdapter.CategoryViewHolder> {
    private Context context;
    ArrayList<Theater> getListTheater() {
        return listTheater;
    }

    void setListTheater(ArrayList<Theater> listTheater) {
        this.listTheater = listTheater;
    }
    private ArrayList<Theater> listTheater;

    ListTheaterAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_theater, parent, false);
        return new CategoryViewHolder(itemRow);
    }
    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final Theater tht = getListTheater().get(position);
        holder.tvName.setText(getListTheater().get(position).getName());
        Glide.with(context)
                .load(getListTheater().get(position).getLogo())
                .override(110, 55)
                .crossFade()
                .into(holder.imgPhoto);
    }
    @Override
    public int getItemCount() {
        return getListTheater().size();
    }
    class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        ImageView imgPhoto;
        CategoryViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tv_item_name);
            imgPhoto = (ImageView)itemView.findViewById(R.id.img_item_photo);
        }
    }
}
