package com.afinahy1605201.utsmobpro.model;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afinahy1605201.utsmobpro.R;
import com.afinahy1605201.utsmobpro.ViewTheater;
import com.bumptech.glide.Glide;

public class MovieDetail extends AppCompatActivity {
    private static final String BASE_URL_PHOTO = "https://image.tmdb.org/t/p/w500";
    public static String EXTRA_NAME = "extra_name";
    public static String EXTRA_RATING = "extra_rating";
    public static String EXTRA_RELEASE = "extra_release";
    public static String EXTRA_DETAIL = "extra_detail";
    public static String EXTRA_BACKDROP = "extra_backdrop";
    public static String EXTRA_POSTER = "extra_poster";
    private TextView tvDataJudul, tvDataRating, tvDataRelease, tvDataDetail;
    private ImageView imgItemBackdrop,imgItemPoster;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        final String name = getIntent().getStringExtra(EXTRA_NAME);
        final String release = getIntent().getStringExtra(EXTRA_RELEASE);
        final String rating = getIntent().getStringExtra(EXTRA_RATING);
        final String detail = getIntent().getStringExtra(EXTRA_DETAIL);
        final String backdrop = getIntent().getStringExtra(EXTRA_BACKDROP);
        final String poster = getIntent().getStringExtra(EXTRA_POSTER);

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

        tvDataJudul = (TextView)findViewById(R.id.tv_item_name);
        tvDataJudul.setText(name);

        tvDataDetail = (TextView)findViewById(R.id.tv_item_overview);
        tvDataDetail.setText(detail);

        imgItemBackdrop = (ImageView)findViewById(R.id.img_item_photo);
        imgItemBackdrop.setColorFilter(filter);

        imgItemPoster = (ImageView)findViewById(R.id.img_item_poster);
        Glide.with(this)
                .load(BASE_URL_PHOTO + backdrop)
                .into(imgItemBackdrop);

        Glide.with(this)
                .load(BASE_URL_PHOTO + poster)
                .into(imgItemPoster);

        Button buttonTheater = (Button)findViewById(R.id.btn_theater);
        buttonTheater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MovieDetail.this, ViewTheater.class);
                intent.putExtra(MovieDetail.EXTRA_NAME, name);
                intent.putExtra(MovieDetail.EXTRA_RATING, rating);
                intent.putExtra(MovieDetail.EXTRA_RELEASE, release);
                intent.putExtra(MovieDetail.EXTRA_DETAIL, detail);
                intent.putExtra(MovieDetail.EXTRA_BACKDROP, backdrop);
                intent.putExtra(MovieDetail.EXTRA_POSTER, poster);
                MovieDetail.this.startActivity(intent);
            }
        });




    }
}
