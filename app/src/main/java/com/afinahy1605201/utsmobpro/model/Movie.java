package com.afinahy1605201.utsmobpro.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Movie implements Parcelable {
    @SerializedName("title")
    private String name;

    @SerializedName("release_date")
    private String release;

    @SerializedName("vote_average")
    private String rating;

    @SerializedName("poster_path")
    private String photo;

    @SerializedName("overview")
    private String detail;

    @SerializedName("backdrop_path")
    private String backdrop;


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getRelease(){
        return release;
    }

    public void setRelease(String release){
        this.release = release;
    }

    public String getRating(){
        return rating;
    }

    public void setRating(String rating){
        this.rating = rating;
    }

    public String getPhoto() { return photo; }

    public void setPhoto(String photo) { this.photo = photo; }

    public String getBackdrop() { return backdrop; }

    public void setBackdrop(String backdrop) { this.backdrop = backdrop; }

    public Movie(String name, String detail, String release, String rating, String photo, String backdrop) {

        this.name = name;
        this.detail = detail;
        this.release = release;
        this.rating = rating;
        this.photo = photo;
        this.backdrop = backdrop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.release);
        dest.writeString(this.rating);
        dest.writeString(this.photo);
        dest.writeString(this.detail);
        dest.writeString(this.backdrop);
    }

    protected Movie(Parcel in) {
        this.name = in.readString();
        this.release = in.readString();
        this.rating = in.readString();
        this.photo = in.readString();
        this.detail = in.readString();
        this.backdrop = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
