package com.afinahy1605201.utsmobpro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.afinahy1605201.utsmobpro.api.MovieApiService;
import com.afinahy1605201.utsmobpro.api.RetrofitClientInstance;
import com.afinahy1605201.utsmobpro.model.Movie;
import com.afinahy1605201.utsmobpro.model.MovieResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afinahy1605201.utsmobpro.api.RetrofitClientInstance.API_KEY;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();


    private CardViewMovieAdapter adapter;
    private RecyclerView recyclerView = null ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_category);
        recyclerView.setHasFixedSize(true);


        MovieApiService service = RetrofitClientInstance.getRetrofitInstance().create(MovieApiService.class);
        Call<MovieResponse> call = service.getPopularMovies(API_KEY);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                showRecyclerCardView(response.body().getResults());
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void showRecyclerCardView(List<Movie> listMovie){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CardViewMovieAdapter(this);
        adapter.setListMovie(listMovie);
        recyclerView.setAdapter(adapter);
    }
}
